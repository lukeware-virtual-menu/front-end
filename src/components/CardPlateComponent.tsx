import Image from "next/image";
import { useState } from "react";
import { getNumberFormatter } from "@/utils/Money";
import Separate from "./SeparateComponet";
import { ItemPlace } from "@/types/ItemPlace";
import ModalComponent from "./ModalComponent";
import CardPlateEditComponent from "./CardPlateEditComponent";

type Props = {
  item: ItemPlace;
  onClickConfirm: (item: ItemPlace) => void;
};

const CardPlateComponent = ({
  item = {
    id: "",
    image: "",
    houseSpecialty: false,
    title: "",
    description: "",
    price: 0,
    category: "",
    quantity: 0,
    enable: true,
  },
}: Props) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleModalOpen = () => {
    setIsModalOpen(true);
    document.body.classList.add("modal-open");
  };

  const handleModalClose = () => {
    setIsModalOpen(false);
    document.body.classList.remove("modal-open");
  };

  const quantityElement = () => {
    if (item.quantity > 0) {
      return (
        <>
          <div className="bg-gray-900 rounded-md w-14 h-10 flex justify-center items-center">
            <span className="text-primary-800">+</span>
            <span className="text-white">{item.quantity}</span>
          </div>
        </>
      );
    }
    return <></>;
  };
  type MyImageProps = {
    src: string;
    width: number;
  };

  return (
    <div className="snap-center shrink-0">
      <div className="shrink-0 rounded-md w-72 bg-white">
        <div className="rounded-md overflow-hidden shadow-lg shadow-black/10">
          <Image
            priority={true}
            loader={(p: any) => `${p.src}&w=${p.width}`}
            alt={item.title}
            src={`${item.image}&s=200`}
            width={300}
            height={130}
            className="w-full object-cover h-32"
          />
          <div className="flex flex-col mx-4">
            <span className="text-lg font-semibold mt-2">{item.title}</span>
            <span className="flex items-center justify-center text-xs font-light mt-1 rounded-full bg-primary-200 text-white w-28 px-2 py-1">
              {item.houseSpecialty && <>House specialty</>}
            </span>
            <span className="text-base text-justify mt-4 text-gray-900">
              {item.description}
            </span>
            <span className="text-xl font-semibold  mt-4 mb-4">
              <span className="text-gray-800">A</span>
              <span className="text-primary-800">
                {getNumberFormatter(item.price)}
              </span>
            </span>
          </div>
          <Separate />
          <div className="flex flex-row items-center justify-between space-x-1 px-4 py-4">
            <button
              title="Order"
              className="bg-primary-800 w-full h-10 rounded-md"
              onClick={handleModalOpen}
            >
              <span className="text-white">Order</span>
            </button>
            {quantityElement()}
          </div>
        </div>
      </div>
      <ModalComponent isOpen={isModalOpen} onClose={handleModalClose}>
        <CardPlateEditComponent
          onClickConfirm={(i: ItemPlace) => {}}
          handleModalClose={handleModalClose}
          item={item}
        ></CardPlateEditComponent>
      </ModalComponent>
    </div>
  );
};

export default CardPlateComponent;
