import Image from "next/image";
import { useState } from "react";
import { getNumberFormatter } from "@/utils/Money";
import { ItemPlace } from "@/types/ItemPlace";
import Separate from "./SeparateComponet";
import { useAppContext } from "@/hooks/UseAppContext";

type Props = {
  item: ItemPlace;
  onClickConfirm: (item: ItemPlace) => void;
  handleModalClose: () => void;
};

const CardPlateEditComponent = ({
  item = {
    id: "",
    image: "",
    houseSpecialty: false,
    title: "",
    description: "",
    price: 0,
    category: "",
    quantity: 0,
    enable: true,
  },
  handleModalClose = () => {},
}: Props) => {
  const [quantity, setQuantity] = useState(1);
  const [note, setNote] = useState("");
  const { updateResume } = useAppContext();

  const AddPlate = () => {
    item.quantity = quantity;
    item.note = note;
    updateResume(item);
  };

  const quantityMinDisable = () => {
    if (quantity <= 1) {
      return "text-gray-400";
    }
    return "text-primary-800";
  };

  return (
    <div className="mx-4">
      <div className="flex justify-start items-center">
        <button onClick={handleModalClose} className="mt-4">
          <div className="flex items-center justify-center">
            <i className="material-symbols-rounded text-primary-800 text-4xl">
              navigate_before
            </i>
            <span className="text-lg">Back</span>
          </div>
        </button>
      </div>
      <div className="flex justify-center items-center">
        <span className="text-xl">Add to order</span>
      </div>
      <div className="rounded-md w-full bg-white pt-4">
        <div className="rounded-md overflow-hidden">
          <div className="px-4">
            <Image
              loader={(p: any) => `${p.src}&width=${p.width}`}
              alt={item.title}
              src={`${item.image}`}
              width={300}
              height={130}
              className="w-full object-cover rounded-md"
              priority={true}
            />
          </div>
          <div className="flex flex-col mx-4">
            <span className="text-xl font-semibold mt-2">{item.title}</span>
            <span className="flex items-center justify-center text-xs font-light mt-1 rounded-full bg-primary-200 text-white w-28 px-2 py-1">
              {item.houseSpecialty && <>House specialty</>}
            </span>
            <span className="text-base text-justify mt-4 text-gray-800">
              {item.description}
            </span>
            <span className="text-xl font-semibold  mt-2">
              <span className="text-gray-800">A</span>
              <span className="text-gray-600">
                {getNumberFormatter(item.price)}
              </span>
            </span>
          </div>
          <Separate />
          <div className="flex flex-col items-start justify-start mx-4">
            <span className="text-base my-1">Note</span>
            <textarea
              value={note}
              onChange={(event) => setNote(event.target.value)}
              title="note"
              className="w-full px-3 py-2 rounded-md h-20 bg-gray-100  placeholder-gray-400 border focus:outline-none focus:border-primary-800 focus:bg-white focus:border-1"
            ></textarea>
          </div>
          <Separate />
          <div className="flex flex-row items-center justify-between mx-4 space-x-4 mb-4">
            <div className="relative flex flex-row items-center justify-between w-1/2">
              <button className={` z-10 ${quantityMinDisable()}`}>
                <i
                  className="material-symbols-rounded text-3xl"
                  onClick={() => {
                    if (quantity - 1 >= 1) {
                      setQuantity(quantity - 1);
                    }
                  }}
                >
                  remove
                </i>
              </button>
              <input
                value={quantity}
                type="numeric"
                disabled={true}
                placeholder="Qt"
                title="quantity"
                className=" absolute text-center w-full z-0 text-sm h-9 px-2 rounded-md bg-gray-50 focus:outline-none  opacity-50 cursor-not-allowed"
              />
              <button className="z-10 text-primary-800">
                <i
                  className="material-symbols-rounded text-3xl"
                  onClick={() => setQuantity(quantity + 1)}
                >
                  add
                </i>
              </button>
            </div>
            <button
              onClick={() => {
                AddPlate();
                handleModalClose();
              }}
              title="confirme"
              className="bg-primary-800 h-10 rounded-md text-white w-1/2 "
            >
              Confirm
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardPlateEditComponent;
