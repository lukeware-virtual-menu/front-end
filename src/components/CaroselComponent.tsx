import { ReactNode } from "react";

type Props = {
  title: string;
  children: ReactNode;
};

const CarouselComponent = ({ title, children }: Props) => {
  return (
    <>
      <span className="flex items-center justify-center text-2xl font-bold">
        {title}
      </span>
      <div className="flex overflow-auto gap-2 overflow-transparent snap-mandatory snap-x py-4">
        <div className="snap-center shrink-0">
          <div className="shrink-0 w-14"></div>
        </div>

        {children}

        <div className="snap-center shrink-0 ">
          <div className="shrink-0 w-14"></div>
        </div>
      </div>
    </>
  );
};

export default CarouselComponent;
