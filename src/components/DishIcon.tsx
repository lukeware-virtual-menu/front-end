type Props = {
  quantity?: number;
};

const DishIcon = ({ quantity }: Props) => {
  return (
    <div className="flex items-center justify-center flex-col bg-primary-800 rounded-md h-8 w-8 shadow-sm shadow-black/20">
      <span className="absolute top-6 text-min text-primary-800">
        {quantity}
      </span>
      <svg
        width="28"
        height="28"
        fill="none"
        version="1.1"
        viewBox="0 0 32 32"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g transform="matrix(1.0035 0 0 1 -.056147 0)">
          <g transform="matrix(1.1049 0 0 1.1049 -1.4353 .38104)" fill="#fff">
            <path
              d="m18.76 18.483c-2.0723-0.01419-3.0985-0.01419-3.8569-0.01419-0.49368 0-0.96813 0.71152-0.19647 0.96119 0 0 3.1959 0.67722 3.4045 1.0376 0.20864 0.36035-3.0936 0.29187-4.1944 0.20662-1.1017-0.08621-4.84-1.6824-4.84-1.6824s-0.75987-0.37755-1.1771 0.17136c-0.32265 0.42434 0.21493 0.8426 0.21493 0.8426l5.9149 3.455h7.5743l0.66109 0.78106h0.27275l0.72765-4.6063c-5.32e-4 -3.21e-4 -2.4948-1.1378-4.5051-1.1525"
              strokeWidth="1.0683"
            />
            <path
              d="m23.99 19.167-0.94655 6.0194 4.3762 0.93138v-6.4517z"
              strokeWidth="1.0683"
            />
            <path
              d="m24.82 17.609v-0.538h1.64v-0.8745h2.0421v-1.574h-24.617v1.574h2.0415v0.8745h1.6405v0.538h17.252z"
              strokeWidth="1.1108"
            />
            <path
              d="m17.97 4.6957c-0.1965-1.2214-1.0625-2.1409-2.1041-2.1409-1.0409 0-1.9076 0.92001-2.1035 2.1409-4.5852 0.87589-8.0278 4.5506-8.031 8.9548h20.271c-0.0044-4.4042-3.4465-8.0796-8.0314-8.9548h-1.17e-4z"
              strokeWidth="1.178"
            />
          </g>
        </g>
      </svg>
    </div>
  );
};

export default DishIcon;
