import { useAppContext } from "@/hooks/UseAppContext";
import Image from "next/image";
import { useEffect, useState } from "react";
import DishIcon from "./DishIcon";



type Props = {
  onClickMenu: () => void;
  onClickOrders: () => void;
};

const NavbarsComponent = ({ onClickMenu, onClickOrders }: Props) => {
  const { quantityTotal } = useAppContext();
  const [isNavFixed, setIsNavFixed] = useState(false);

  const navClass = isNavFixed ? "fixed top-0 left-0 right-0" : "";

  useEffect(() => {
    const handleScroll = () => {
      if (window.pageYOffset > 0) {
        setIsNavFixed(true);
      } else {
        setIsNavFixed(false);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <>
      <div
        className={`w-full flex justify-between items-center bg-white px-4 shadow-md z-10 ${navClass}`}
      >
        <button
          title="menu"
          className="flex items-center"
          onClick={onClickMenu}
        >
          <div className="flex items-center justify-center rounded-md">
            <i className="material-symbols-rounded font-bold text-primary-700 hover:text-primary-800 active:text-primary-900 p-1">
              menu
            </i>
          </div>
        </button>

        <Image
          alt="logo"
          src={"/logo.png"}
          width={250}
          height={100}
          priority={true}
          className="w-32"
        />

        <button
          title="dish"
          className="flex items-center"
          onClick={onClickOrders}
        >
          <div className="flex items-center justify-center">
            <DishIcon quantity={quantityTotal} />
          </div>
        </button>
      </div>
    </>
  );
};

export default NavbarsComponent;
