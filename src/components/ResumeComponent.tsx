import { useAppContext } from "@/hooks/UseAppContext";
import { saveOrder } from "@/service/order_service";
import { ItemPlace } from "@/types/ItemPlace";
import { getNumberFormatter } from "@/utils/Money";
import Separate from "./SeparateComponet";

const ResumeComponent = () => {
  const { resume, quantityTotal, tableCode, setResume, setQuantityTotal } =
    useAppContext();

  const handleSaveOrder = () => {
    resume.tableCode = tableCode;
    saveOrder(resume);
    setResume({
      items: [],
      subTotal: 0,
      note: "",
      tableCode: "",
    });
    setQuantityTotal(0);
    alert("Save order!");
  };

  const existQuantityTotal = () => {
    if (quantityTotal > 0) {
      return (
        <div className="bg-primary-800 mx-4 my-4 rounded-md text-white py-4">
          <div className="flex flex-col">
            <div className="flex items-center justify-between mx-4 mb-2 text-lg font-bold">
              <span>Resume</span>
              <button title="edit">
                <i className="material-symbols-rounded">edit_note</i>
              </button>
            </div>

            {resume &&
              resume.items
                .filter((it: ItemPlace) => it.quantity > 0)
                .map((it: ItemPlace) => (
                  <div
                    key={it.id}
                    className="flex items-center justify-between px-4 py-1"
                  >
                    <div className="text-xs shrink-0 mr-1">{`${it.title} (${it.quantity}x)`}</div>
                    <div className="w-full border-t border-gray-300/50"></div>
                    <div className="text-xs ml-1">
                      {getNumberFormatter(it.price * it.quantity)}
                    </div>
                  </div>
                ))}

            <Separate />
            <div className="flex items-center justify-between px-4 py-1">
              <div className="text-lg shrink-0 mr-1 font-semibold">
                Subtotal
              </div>
              <div className="w-full border-t border-gray-300/50"></div>
              <div className="text-lg font-semibold ml-1">
                {getNumberFormatter(resume.subTotal)}
              </div>
            </div>
            <div className="flex items-center justify-between px-4 mb-6">
              <div className="text-lg shrink-0 mr-1 font-semibold">Table</div>
              <div className="w-full border-t border-gray-300/50"></div>
              <div className="text-lg font-semibold ml-1">#{tableCode}</div>
            </div>
            <button
              title="confirmOrder"
              onClick={handleSaveOrder}
              className="bg-white text-gray-900 mx-4 h-10 rounded-md"
            >
              <span className="uppercase">Place My Order</span>
            </button>
          </div>
        </div>
      );
    }
  };

  return <>{existQuantityTotal()}</>;
};

export default ResumeComponent;
