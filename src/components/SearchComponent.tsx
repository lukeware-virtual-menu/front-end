import React, { FC, InputHTMLAttributes } from "react";

const SearchComponent = ({ ...props }: InputHTMLAttributes<HTMLInputElement>) => (
  <>
    <div className="flex justify-center mx-4 mt-4">
      <div className="relative">
        <span className="absolute inset-y-0 left-0 flex items-center pl-2">
          <i className="material-symbols-rounded text-gray-400 text-xl font-semibold">
            search
          </i>
        </span>
      </div>
      <input
        className="w-full text-sm bg-gray-300 h-9 pl-8 px-4 py-2 rounded-md text-gray-500 focus:bg-gray-200 placeholder-gray-400 border focus:outline-none focus:border-primary-800 focus:border-2"
        {...props}
      />
    </div>
  </>
);

export default SearchComponent;
