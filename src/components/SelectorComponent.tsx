import { Fragment, useState } from "react";
import { Listbox, Transition } from "@headlessui/react";
import { Option } from "@/types/Option";


type Props = {
  options: Option[];
  selectedOption?: any;
  onOptionSelected: (option: any) => void;
};

function classNames(...classes: any[]) {
  return classes.filter(Boolean).join(" ");
}

const SelectorComponent = ({
  options = [],
  selectedOption = "",
  onOptionSelected,
}: Props) => {
  return (
    <Listbox value={selectedOption} onChange={onOptionSelected}>
      {({ open }) => (
        <>
          <div className="relative mx-4 my-4">
            <Listbox.Button className="relative w-full cursor-default rounded-md bg-white py-1.5 pl-3 pr-10 text-left text-gray-900 ring-inset ring-0 focus:outline-none focus:ring-2 focus:ring-primary-700 sm:text-sm sm:leading-6 font-medium">
              <span className="flex items-center">
                <span className="ml-1 block truncate">
                  {selectedOption.description}
                </span>
              </span>
              <span className="pointer-events-none absolute inset-y-0 right-0 ml-3 flex items-center pr-2"></span>
            </Listbox.Button>

            <Transition
              show={open}
              as={Fragment}
              leave="transition ease-in duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Listbox.Options className="absolute  z-10 mt-1 max-h-56 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                {options.map((person) => (
                  <Listbox.Option
                    key={person.id}
                    className={({ active }) =>
                      classNames(
                        active
                          ? "bg-gray-50 text-primary-700 font-medium "
                          : "text-gray-500",
                        "relative cursor-default select-none py-2 pl-3 pr-9"
                      )
                    }
                    value={person}
                  >
                    {({ selected, active }) => (
                      <>
                        <div className="flex items-center">
                          <span
                            className={classNames(
                              selected ? "font-semibold" : "font-normal",
                              "ml-3 block truncate"
                            )}
                          >
                            {person.description}
                          </span>
                        </div>

                        {selected ? (
                          <span
                            className={classNames(
                              active ? "text-white" : "text-primary-700",
                              "absolute inset-y-0 right-0 flex items-center pr-4"
                            )}
                          >
                            <i className="material-symbols-rounded text-primary-700 text-xl font-semibold">
                              check_small
                            </i>
                          </span>
                        ) : null}
                      </>
                    )}
                  </Listbox.Option>
                ))}
              </Listbox.Options>
            </Transition>

            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-primary-800">
              <span className="material-symbols-rounded">expand_more</span>
            </div>
          </div>
        </>
      )}
    </Listbox>
  );
};

export default SelectorComponent;
