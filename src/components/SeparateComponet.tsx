const Separate = () => {
  return (
    <div className="border-b border-dashed border-gray-300 border-opacity-50 my-3"></div>
  );
};

export default Separate;
