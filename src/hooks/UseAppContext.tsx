import { ItemPlace } from "@/types/ItemPlace";
import { Plate } from "@/types/Plate";
import { Resume } from "@/types/Resume";
import { createContext, useContext, useState } from "react";

interface AppContextProps {
  quantityTotal: number;
  setQuantityTotal: (quantityTotal: number) => void;

  tableCode: string;
  setTableCode: (tableCode: string) => void;

  resume: Resume;
  setResume: (resume: Resume) => void;

  menuItems: ItemPlace[];
  setMenuItems: (menuItems: ItemPlace[]) => void;

  updateResume: (item: ItemPlace) => void;
}

export const AppContext = createContext<AppContextProps>({
  quantityTotal: 0,
  setQuantityTotal: () => {},

  tableCode: "0",
  setTableCode: () => {},

  resume: {
    items: [],
    subTotal: 0,
    note: "",
    tableCode: "",
  },
  setResume: (resume: Resume) => {},

  menuItems: [],
  setMenuItems: (menuItems: ItemPlace[]) => {},

  updateResume: (item: ItemPlace) => {},
});

export const useAppContext = () => useContext(AppContext);

const AppProvider = ({ children }: any) => {
  const [quantityTotal, setQuantityTotal] = useState(0);
  const [tableCode, setTableCode] = useState("");
  const [menuItems, setMenuItems] = useState<ItemPlace[]>([]);
  const [resume, setResume] = useState<Resume>({
    items: [],
    subTotal: 0,
    note: "",
    tableCode: "",
  });

  const updateResume = (item: ItemPlace) => {
    setMenuItems(
      menuItems.map((it) => {
        if (it.id === item.id) {
          resume.items.push(item);
          setResume(resume);
          return {
            ...it,
            quantity: item.quantity,
            note: item.note,
          };
        }
        return it;
      })
    );

    let quantityTotal = 0;
    resume.items.forEach((it) => {
      quantityTotal += it.quantity;
    });

    setQuantityTotal(quantityTotal);

    let subTotal = 0;
    resume.items.forEach((it) => {
      subTotal += it.quantity * it.price;
    });

    resume.subTotal = subTotal;
    setResume(resume);
  };

  return (
    <AppContext.Provider
      value={{
        quantityTotal,
        setQuantityTotal,

        tableCode,
        setTableCode,

        resume,
        setResume,

        menuItems,
        setMenuItems,

        updateResume,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export default AppProvider;
