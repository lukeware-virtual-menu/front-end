import type { NextApiRequest, NextApiResponse } from "next";

const url = process.env.BACKOFFICE_URL;
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const requestOptions: RequestInit = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(req.body),
    redirect: "follow",
  };

  try {
    await fetch(`${url}/orders`, requestOptions);
    res.status(200).json({});
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: "Failed to save order" });
  }
}
