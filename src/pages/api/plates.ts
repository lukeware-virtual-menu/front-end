import type { NextApiRequest, NextApiResponse } from "next";
const url = process.env.BACKOFFICE_URL;
export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const requestOptions: RequestInit = {
    method: "GET",
    redirect: "follow",
  };

  try {
    const response = await fetch(
      `${url}/plates?page=1&limit=1000`,
      requestOptions
    );
    const plates = await response.json();
    res.setHeader("Cache-Control", "s-maxage=86400");
    res.status(200).json(plates.data);
  } catch (error) {
    res.status(500).json({ error: "Failed to list plates" });
    console.error(error);
  }
}
