import { ItemPlace } from "@/types/ItemPlace";
import { Option } from "@/types/Option";
import { Plate } from "@/types/Plate";
import { toItemPlate } from "@/utils/Convert";
import { equalsIgnoreCase } from "@/utils/Equals";
import { GetServerSideProps } from "next";
import { Lexend } from "next/font/google";
import { useEffect, useState } from "react";
import { useAppContext } from "@/hooks/UseAppContext";
import { useRouter } from "next/router";
import CarouselComponent from "@/components/CaroselComponent";
import CardPlateComponent from "@/components/CardPlateComponent";
import NavbarsComponent from "@/components/NavbarsComponent";
import SelectorComponent from "@/components/SelectorComponent";
import ResumeComponent from "@/components/ResumeComponent";

const inter = Lexend({
  subsets: ["latin"],
  style: ["normal"],
  preload: true,
});

const options: Option[] = [
  {
    id: 1,
    description: "All",
  },
  {
    id: 2,
    description: "Staters",
  },
  {
    id: 3,
    description: "Tacos",
  },
  {
    id: 4,
    description: "Burritos",
  },
];

const Home = ({ plates }: ProductsPageProps) => {
  const [selectedOption, setSelectedOption] = useState(options[0]);
  const {
    quantityTotal,
    setQuantityTotal,
    setTableCode,
    setMenuItems,
    menuItems,
  } = useAppContext();
  const router = useRouter();
  const { tableCode } = router.query;

  const onClickConfirm = (item: ItemPlace) => {
    setQuantityTotal(quantityTotal + item.quantity);
  };

  const caroselSelect = (option: Option) => {
    return (
      <CarouselComponent title={option.description} key={option.id}>
        {menuItems &&
          menuItems
            .filter((it) => equalsIgnoreCase(it.category, option.description))
            .filter((it) => it.enable)
            .map((it) => (
              <CardPlateComponent
                key={it.id}
                item={it}
                onClickConfirm={onClickConfirm}
              />
            ))}
      </CarouselComponent>
    );
  };

  const caroselSelectAll = () => {
    return options
      .filter((it) => !equalsIgnoreCase(it.description, "All"))
      .map((it) => caroselSelect(it));
  };

  useEffect(() => {
    setMenuItems(plates.map((plate) => toItemPlate(plate)));
  }, [plates, setMenuItems]);

  useEffect(() => setTableCode(typeof tableCode === "string" ? tableCode : ""));

  const [isNavFixed, setIsNavFixed] = useState(false);
  const divClass = isNavFixed ? "mt-20" : "";

  useEffect(() => {
    const handleScroll = () => {
      if (window.pageYOffset > 0) {
        setIsNavFixed(true);
      } else {
        setIsNavFixed(false);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <main className={`${inter.className} select-none`}>
      <NavbarsComponent onClickOrders={() => {}} onClickMenu={() => {}} />
      <div className={`${divClass}`}>
        <SelectorComponent
          options={options}
          selectedOption={selectedOption}
          onOptionSelected={(option: Option) => setSelectedOption(option)}
        />

        {!equalsIgnoreCase(selectedOption.description, "All") &&
          caroselSelect(selectedOption)}

        {equalsIgnoreCase(selectedOption.description, "All") &&
          caroselSelectAll()}

        <ResumeComponent />
      </div>
    </main>
  );
};

type ProductsPageProps = {
  plates: Plate[];
};

export const getServerSideProps: GetServerSideProps<
  ProductsPageProps
> = async () => {
  try {
    const res = await fetch("http://192.168.15.21:3000/api/plates");
    const plates = await res.json();
    return { props: { plates: plates } };
  } catch (error) {
    return { props: { plates: [] } };
  }
};

export default Home;
