import { Resume } from "@/types/Resume";

export const saveOrder = async (order: Resume): Promise<void> => {
  try {
    const res = await fetch(`/api/orders`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(order),
    });
    if (!res.ok) {
      throw new Error("Failed to save order");
    }
  } catch (error) {
    console.error(error);
    throw error;
  }
};
