export type ItemPlace = {
  id: string;
  image: string;
  houseSpecialty: boolean;
  title: string;
  description: string;
  price: number;
  category: string;
  quantity: number;
  note?: string;
  enable: boolean;
};
