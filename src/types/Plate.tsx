export type Plate = {
  id: string;
  image: string;
  houseSpecialty: boolean;
  title: string;
  description: string;
  price: number;
  category: string;
  enable: boolean;
};
