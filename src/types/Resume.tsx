import { ItemPlace } from "./ItemPlace";

export type Resume = {
  items: ItemPlace[];
  subTotal: number;
  note: string;
  tableCode: string;
};
