import { ItemPlace } from "@/types/ItemPlace";
import { Plate } from "@/types/Plate";

export const toItemPlate = (plate: Plate): ItemPlace => {
  return {
    category: plate.category,
    description: plate.description,
    houseSpecialty: plate.houseSpecialty,
    image: plate.image,
    id: plate.id,
    price: plate.price,
    quantity: 0,
    title: plate.title,
    enable: plate.enable,
  };
};
