export const equalsIgnoreCase = (str1: string, str2: string): boolean => {
  const normalize = (s: string) =>
    (s && s.toLowerCase().replace(/[^a-z0-9]/gi, "")) || "";
  return normalize(str1) === normalize(str2);
};
