export const getNumberFormatter = (number: number) => {
  const num = Math.floor(number * 100) / 100;
  return parseFloat(num.toFixed(2)).toLocaleString("en-AU", {
    style: "currency",
    currency: "AUD",
  });
};